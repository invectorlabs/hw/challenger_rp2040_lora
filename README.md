# The Challenger RP2040 LoRa/SubGHz
![Challenger RP2040 LoRa](https://ilabs.se/wp-content/uploads/2022/02/iso1-11-scaled.jpg)

## Challenger RP2040 LoRa/SUBGhz information
The Challenger RP2040 LoRa board is an Arduino/Circuitpython compatible Adafruit Feather format micro controller board packed with loads of functionality for your projects that require low power consumption and a BLE connection.

The main controller of this board is the Raspberry Pi Pico (RP2040) with 8MByte of FLASH memory and 264KByte of SRAM. The RP2040 MCU is built around the dual 32-bit ARM® Cortex™-M0 CPU's running at up to 133 MHz. It has numerous digital peripherals and interfaces such as high speed SPI and QSPI for interfacing to external flash and a full speed USB device for data transfer and power supply for battery recharging.

## Challenger RP2040 LoRa/SubGHz Key features
- Dual ARM® Cortex M0 running at up to 133MHz
- 8 MB Flash, 264 KB RAM
- 868/915 MHz RFM95W LoRa transceiver or RFM69HVW SubGHz radio transceiver
- UART, SPI, TWI, I2S, QSPI
- PWM
- 12-bit ADC
- USB 2.0

## The on board RF module

This board can be equiped with either the RF95W LoRa module making it a LoRaWAN capable micro controller board. But it can also be equiped with the RFM69HCW SubGHz radio module allowing users to create their own low power radio networks.

 - ![RFM95W information](https://www.hoperf.com/modules/lora/RFM95.html)
 - ![RFM69HCW information](https://www.hoperf.com/modules/rf_transceiver/RFM69HCW.html)

The same PCB is used for both versions of the board.

### Power
There is an onboard low power LDO that runs the onboard electronic devices. 
The board also comes with a connector for external LiPo batteries as well as a LiPo charger circuit that.

### Other stuff
It also has the Challenger standard USB type C connector with ajoining LED's. A red LED for the charging circuit indicating when the attached battery is being charged and a green user programmable LED. In addition to this there is also a blue LED that can be used for signaling RF activity, or anything else really, to the user. The UF2 boot loader uses this LED to indicate that it is waiting for your code to be uploaded. Additionally there is also a user programmable tactile switch that can be used for user input. This button is also used by the boot loader to enter DFU mode.

Anf of course, there is a reset switch. You can not survive without the reset switch =)

### Pinout
<img src="https://ilabs.se/wp-content/uploads/2022/02/challenger-rp2040-lora-pinout-diagram-v0.1.png" alt="drawing"/>

### Software support
The board is supported by both the Arduino environment as well as Circuitpython from Adafruit.

## License
This design covered by the CERN Open Hardware Licence v1.2 and a copy of this license is also available in this repo.

## iLabs
<img src="https://ilabs.se/wp-content/uploads/2021/07/cropped-Color-logo-no-background.png" alt="drawing" width="200"/>

### About us
Invector Labs is a small Swedish engineering company that designs and build electronic devices for hobbyists as well as small and medium sized businesses.

For more information about this board you can visit the product page at our [website](https://ilabs.se/product/challenger-840-ble/)

Questions about this product can be addressed to <oshwa@ilabs.se>.

/* EOF */

